const { Kafka } = require("kafkajs")
const { MongoClient } = require('mongodb')
const express = require("express")
const bodyParser = require("body-parser")

const app = express() // create express application
const port = 8000 // port for application to listen on

const dbUri = "mongodb://db:27017/mydb" // mongodb connection uri
let dbClient

// client configuration for kafka connection
const kafka = new Kafka({
  clientId: 'my-app',
  brokers: ['kafka:9092'],
  retry: {
    retries: 10 // increase number of retries from the default 5 to 10
  }
})

// this function creates a kafka consumer that subscribes to
// the "purchases" topic and inserts each message that it
// consumes to the database
const consume = async () => {
  consumer = kafka.consumer({ groupId: 'my-group' })
  
  await consumer.connect()
  await consumer.subscribe({ topic: 'purchases', fromBeginning: true })
  
  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        value: message.value.toString(),
      })
      try {
        dbClient = new MongoClient(dbUri)
        const database = dbClient.db("mydb")
        const purchaseCollection = database.collection("purchases")
        const result = await purchaseCollection.insertOne(JSON.parse(message.value.toString()));
        console.log(`A document was inserted with the _id: ${result.insertedId}`)
      } catch (error) {
        console.log(error)
      } finally {
        await dbClient.close();
      }
    },
  })
}

// route for get request to the path "/buyList", which includes
// a middleware function that queries the db for all the purchases
// made by the user that is sent in the query string
app.get("/buyList", bodyParser.json(), async (req, res) => {
  dbClient = new MongoClient(dbUri)
  const database = dbClient.db("mydb")
  const purchaseCollection = database.collection("purchases")
  const query = {username:req.query.user};
  const result = await purchaseCollection.find(query,{projection: {_id: 0}}).toArray()
  res.send({
    result: result
  })
  await dbClient.close();
})

// bind and listen for connections on the specified port
app.listen(port, () => {
  const timeout = setTimeout(consume, 5000) // delaying the consuming function to let kafka finish startup
  console.log(`App is listening on port ${port}`)
})