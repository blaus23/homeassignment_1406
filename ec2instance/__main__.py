import pulumi
import pulumi_aws as aws

size = 't2.micro' # this is the free tier size, you can choose a larger size for your instance

# get ami of ubuntu image for launching ec2 instance
ami = aws.get_ami(most_recent="true",
                  owners=["099720109477"],
                  filters=[{"name":"name","values":["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20230516"]}]) # filter by name of ami

# create a vpc
vpc = aws.ec2.Vpc('server-vpc', 
    cidr_block='10.0.0.0/16',
)

# create an an internet gateway.
gateway = aws.ec2.InternetGateway('server-gateway',
    vpc_id=vpc.id,
)

# create a subnet that automatically assigns new instances a public IP address.
subnet = aws.ec2.Subnet('server-subnet',
    vpc_id=vpc.id,
    cidr_block='10.0.1.0/24',
    map_public_ip_on_launch=True,
)

# create a route table.
routes = aws.ec2.RouteTable('server-routes',
    vpc_id=vpc.id,
    routes=[
        { 'cidrBlock': '0.0.0.0/0', 'gatewayId': gateway.id }
    ])

# associate the route table with the public subnet.
route_table_association =aws.ec2.RouteTableAssociation('route-table-association',
    subnet_id=subnet.id,
    route_table_id=routes.id,
)

# provides a security group resource to allow http and ssh access to the ec2 instance
group = aws.ec2.SecurityGroup('server-secgrp',
    vpc_id=vpc.id,
    ingress=[
        { 'protocol': 'tcp', 'from_port': 22, 'to_port': 22, 'cidr_blocks': ['0.0.0.0/0'] }, # allow ssh access to all network interfaces
        { 'protocol': 'tcp', 'from_port': 80, 'to_port': 80, 'cidr_blocks': ['0.0.0.0/0'] }, # allow http access on port 80 to all network interfaces
    ],
    egress=[
        { 'protocol': '-1', 'from_port': 0, 'to_port': 0, 'cidr_blocks': ['0.0.0.0/0'] }
    ])

# pass bash script to user_data variable that will be used to configure the ec2 instance at launch time
with open('./ec2runningscript.txt', 'r') as runtime_script:
    user_data = runtime_script.read()

# provides ec2 instance resource to be created and launched
server = aws.ec2.Instance('server-instance',
    instance_type=size,
    vpc_security_group_ids=[group.id],
    ami=ami.id,
    user_data=user_data,
    subnet_id=subnet.id,) 

# export stack outputs of the new ec2 instance's public ip address
pulumi.export('publicIp', server.public_ip)
