# Home Assignment - Ofir Blaus

## Steps for getting setup up & running

### Before you begin
You'll need to have the following:<br>

* [Pulumi](https://www.pulumi.com/docs/install/) and [Python (version 3.7 or later)](https://www.python.org/downloads/) installed on your computer.
* [Create an IAM user in the AWS console](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html#id_users_create_console) and setup AWS credentials for the user. Ensure that the user has Programmatic access with rights to deploy and manage resources handled through this Pulumi project.
* Configure AWS credentials to work with Pulumi, and set the AWS region to us-west-2.

You can configure AWS credentials and region with the following commands:<br>
On Windows:
```Bash
$env:AWS_ACCESS_KEY_ID = "<YOUR_ACCESS_KEY_ID>"
$env:AWS_SECRET_ACCESS_KEY = "<YOUR_SECRET_ACCESS_KEY>"
$env:AWS_REGION = "<YOUR_AWS_REGION>"
```
On Linux/MacOS:
```Bash
export AWS_ACCESS_KEY_ID=<YOUR_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<YOUR_SECRET_ACCESS_KEY>
export AWS_REGION=<YOUR_AWS_REGION>
```

### Deploy stack
1. [Clone this repository](https://gitlab.com/blaus23/homeassignment_1406.git) to your computer.
2. Change directory to `ec2instance`:
```Bash
cd ./homeassignment_1406/ec2instance
```
3. Run `pulumi up`. You can either choose an existing Pulumi stack or create a new one.
4. Once the preview has finished, choose yes out of the three options that are given. It will take about 40 seconds to complete the deployment. Copy the public IP address that is showcased in the output.
5. Wait 5-6 minutes for the EC2 Instance to initialize and for the configuration script to finish running.
6. Open your browser, and search for `http://<PUBLIC_IP_THAT_YOU_COPIED>`
---

## Services
* Frontend: Developed using React and deployed with `serve`.
* Customer Facing Web Server: Developed using Express (NodeJS).
* Nginx: Used as a reverse proxy that forwards requests to either the frontend or the customer facing web server.
* Kafka: Collects data from the customer facing web server. Deployed with KRaft to eliminate the need for a Zookeeper service.
* Customer Management API: Developed using Express (NodeJS). Consumes data from Kafka and sends it to the database.
* MongoDB: Stores the data sent from the customer management API.