const { Kafka } = require('kafkajs')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express() // create express application
const port = 3000 // port for application to listen on
app.use(cors()) // allow cors access

// client configuration for kafka connection
const kafka = new Kafka({
  clientId: 'my-app',
  brokers: ['kafka:9092'],
  retry: {
    retries: 10 // increase number of retries from the default 5 to 10
  }
})

// this function receives an Object that includes purchasing data,
// creates a kafka producer and sends a message containing the received data
const produce = async (data) => {
  const producer = kafka.producer()
  
  await producer.connect()
  await producer.send({
    topic: 'purchases',
    messages: [
      { value: JSON.stringify(data) },
    ],
  })
  
  await producer.disconnect()
}

// route for post request to the path "/newPurchase", which includes
// a middleware function that saves the client's request parameters 
// in an Object that is sent to the produce function 
app.post('/newPurchase', bodyParser.json(), (req, res) => {
  const purchaseData = {
    username: req.body.username,
    items: req.body.items,
    price: req.body.price,
    timestamp: Date.now()
  }
  produce(purchaseData)

  res.status(201).send({
    msg: 'Purchase request has been sent!'
  })
})

// route for get request to the path "/getAllUserBuys", which includes
// a middleware function that queries the management api server for
// all the purchases made by the user that is sent in the client's request
app.get('/getAllUserBuys', bodyParser.json(), async (req, res) => {
  const response = await fetch(`http://api:8000/buyList?user=${req.query.user}`)
  const result = await response.json()
  res.status(200).send(result)
})

// bind and listen for connections on the specified port
app.listen(port, () => {
  console.log(`App is listening on port ${port}`)
})