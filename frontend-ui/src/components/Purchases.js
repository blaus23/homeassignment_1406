import React from 'react';
import '../stylesheets/Purchases.css'

export default function Purchases(props) {

  // map through array of purchases and return an html element for each purchase
  // with it's info (items bought, total price and date of purchase)
  const purchaseElemets = props.purchaseList.map((purchase, i) => {

    // map through array of items purchased and return an html element that
    // represents the item bought and the quantity of the item
    const itemList = Object.keys(purchase.items).map((key, i) => {
      return (
        <div key={i}>
          Fruit: {key}, Quantity: {purchase.items[key]}
        </div>
      )
    })
    const dateOfPurchase = new Date(purchase.timestamp)
    return (
      <div key={i} className='purchase'>
        Items List:<br />
        {itemList}
        <p>Total price: {purchase.price}</p>
        <div className='spacer'></div><br />
        <p className='date'>Date of purchase: {dateOfPurchase.toUTCString()}</p>
      </div>
    )
  })

  return (
    <div className='purchase-list'>
      {purchaseElemets}
    </div>
  )
}