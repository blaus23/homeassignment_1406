import React, { useState } from 'react';
import '../stylesheets/PurchaseConfirmation.css'
import axios from 'axios';

export default function PurchaseConfirmation(props) {
  
  // map through array of items purchased and return an html element that
  // represents the item bought and the quantity of the item
  const purchaseList = Object.keys(props.purchase.items).map((key, i) => {
    return (
      <div key={i}>
        Fruit: {key}, Quantity: {props.purchase.items[key]}
      </div>
    )
  })

  const [username, setUsername] = useState('')
  const [msg, setMsg] = useState('')

  const handleChange = (event) => {
    setUsername(event.target.value)
  }

  // send POST request to customer facing webserver which includes info
  // of the purchase (username of the buyer, items bought and total price)
  const handleBuy = async () => {
    const result = await axios.post('/newPurchase', {
      username: username,
      items: props.purchase.items,
      price: props.purchase.totalPrice
    })
    setMsg(await result.data.msg)
  }

  return (
    <>
      <div className='modal'>
        <div className='modal-content'>
          <span className='close' onClick={props.handleCloseProceed}>&times;</span>
          <p>Purchase List:</p>
          {purchaseList}
          <p>Total price: {props.purchase.totalPrice}</p>
          Enter Username: <input name='username' onChange={handleChange} />
          <button disabled={username.length === 0 || msg.length > 0} onClick={() => handleBuy()}>Buy</button>
          {
            msg.length > 0 ?
            <p>{msg}</p> :
            null
          }
        </div>
      </div>
    </>
  )
}