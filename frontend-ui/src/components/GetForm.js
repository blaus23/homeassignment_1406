import React, { useState } from 'react';
import axios from 'axios';
import Purchases from './Purchases';
import '../stylesheets/GetForm.css'

export default function GetForm() {

  const [username, setUsername] = useState('')
  const [purchaseList, setPurchaseList] = useState([])

  const handleChange = (event) => {
    setUsername(event.target.value)
  }

  // send GET request to customer facing webserver which returns an array
  // of the purchases made by a user
  const handleGetBuys = async () => {
    const result = await axios.get('/getAllUserBuys', {
      params: {
        user: username
      }
    })
    setPurchaseList(await result.data.result)
  }

  return (
    <>
      <div className='get-form'>
        <div className='user-input'>
          Enter Username: <input name='username' onChange={handleChange} />
          <button disabled={username.length === 0} onClick={() => handleGetBuys()}>Get Buys</button>
        </div>
        {
          // load the Purchases component when a non-empty response is received from the
          // customer facing webserver
          purchaseList.length > 0 ?
          <Purchases username={username} purchaseList={purchaseList} /> :
          null
        }
      </div>
    </>
  )
}