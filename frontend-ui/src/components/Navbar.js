import React from 'react';
import '../stylesheets/Navbar.css'

export default function Navbar() {
  return (
    <div className='navbar'>
      <img src='/favicon.ico' alt='icon' />
      <span>Welcome to the Fruit Store!</span>
    </div>
  )
}