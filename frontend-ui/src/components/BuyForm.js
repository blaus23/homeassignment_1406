import React, { useReducer, useState } from 'react';
import products from '../products';
import '../stylesheets/BuyForm.css'
import PurchaseConfirmation from './PurchaseConfirmation';

export default function BuyForm() {

  const [isProceedWithPurchase, setIsProceedWithPurchase] = useState(false)

  // reducer is used to consolidate the purchase update logic in  a single function
  const reducer = (state, action) => {
    switch(action.type) {
      // when adding a new item to the purchase, check if the item is already present on
      // the items Object, and create a key for it if it doesn't with an initial value of 1
      case 'add': {
        if (!(action.name in state.items)) {
          state.items[action.name] = 1
        }
        else {
          state.items[action.name]++
        }
        return {
          ...state,
          totalPrice: state.totalPrice + action.price // add the price of the added item to the total price
        }
      }
      // when removing an item from the purchase, check if the item is present on the
      // items Object, and if it does, subtract 1 from the item's value, or delete it if
      // the value is 1
      case 'subtract': {
        if (action.name in state.items) {
          if (state.items[action.name] > 1) {
            state.items[action.name]--
          }
          else {
            delete state.items[action.name]
          }
          return {
            ...state,
            totalPrice: state.totalPrice - action.price // subtract the price of the removed item from the total price
          }
        }
        else {
          return {
            ...state
          }
        }
      }
      default:
        return state
    }
  }
  
  // the purchase state consists of the total price of the purchase, and an Object that
  // represents the items purchased, with it's keys representing each item and it's values
  // representing the the quantity of each item
  const [state, dispatch] = useReducer(reducer, {
    totalPrice: 0,
    items: {}
  })

  const addItemToBuyList = (item) => {
    dispatch({
      type: 'add',
      name: item.name,
      price: item.price
    })
  }

  const subtractItemFromBuyList = (item) => {
    dispatch({
      type: 'subtract',
      name: item.name,
      price: item.price
    })
  }

  // map through array of products and return an html element for each of the
  // products with their info (name and price)
  const productElements = products.map((product, i) => {
    return (
      <div key={i} className='product'>
        <div>{product.name}</div>
        <img src={product.url} alt={product.name} style={{ width: '25px', }} />
        <div>Price: {product.price}</div>
        <div className='quantity'>
          <span className='set-quantity' onClick={() => subtractItemFromBuyList(product)}>-</span>
          <span>{product.name in state.items ? state.items[product.name] : 0}</span>
          <span className='set-quantity' onClick={() => addItemToBuyList(product)}>+</span>
        </div>
      </div>
    )
  })

  const handleOpenProceed = () => {
    setIsProceedWithPurchase(true)
  }
  const handleCloseProceed = () => {
    setIsProceedWithPurchase(false)
  }

  return (
    <>
      <div className='product-list'>
        {productElements}
      </div>
      <button className='proceed-button' disabled={state.totalPrice === 0} onClick={() => handleOpenProceed()}>Proceed</button>
      {
        // load the PurchaseConfirmation component when the user clicks the "Proceed" button,
        // the button can only be pressed if the purchase contains at least one product
        isProceedWithPurchase ?
        <PurchaseConfirmation handleCloseProceed={handleCloseProceed} purchase={state} /> :
        null
      }
    </>
  )
}