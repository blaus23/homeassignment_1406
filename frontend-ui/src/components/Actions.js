import React, { useState } from 'react'
import '../stylesheets/Actions.css'
import BuyForm from './BuyForm'
import GetForm from './GetForm'

export default function Actions() {

  const [action, setAction] = useState()

  const handleBuy = () => {
    setAction('buy')
  }

  const handleGetAllUserBuys = () => {
    setAction('get')
  }

  return (
    <>
      <div className='buttons'>
        <div className='button' onClick={() => handleBuy()}>
          Buy
        </div>
        <div className='button' onClick={() => handleGetAllUserBuys()}>
          getAllUserBuys
        </div>
      </div>
      {
        // load component based on user choice (BuyForm when user clicks "Buy" button
        // and GetForm when user clicks "getAllUserBuys" button)
        action === 'buy' ?
        <BuyForm />
        : action === 'get' ?
        <GetForm /> :
        null
      }
    </>
  )
}