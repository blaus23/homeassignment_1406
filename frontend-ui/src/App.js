import Actions from './components/Actions';
import Navbar from './components/Navbar';
import './stylesheets/App.css';

function App() {
  return (
    <div className='App'>
      <Navbar />
      <Actions />
    </div>
  );
}

export default App;
