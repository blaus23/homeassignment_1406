const products = [
  {
    name: 'Banana',
    price: 4,
    url: 'https://parspng.com/wp-content/uploads/2023/02/bananapng.parspng.com-2.png'
  },
  {
    name: 'Orange',
    price: 2,
    url: 'https://www.boeschbodenspies.com/wp-content/uploads/2017/08/orange.png'
  },
  {
    name: 'Apple',
    price: 3,
    url: 'https://pngfre.com/wp-content/uploads/apple-98-964x1024.png'
  },
  {
    name: 'Peach',
    price: 5,
    url: 'https://pngfre.com/wp-content/uploads/peach-png-image-from-pngfre-36-266x300.png'
  },
  {
    name: 'Watermelon',
    price: 5.5,
    url: 'https://i.pinimg.com/originals/1e/31/19/1e3119a4ab362877b96e347ffc6f1db0.png'
  },
  {
    name: 'Pineapple',
    price: 7,
    url: 'https://static.vecteezy.com/system/resources/previews/008/848/362/non_2x/fresh-pineapple-free-png.png'
  },
  {
    name: 'Lemon',
    price: 2.5,
    url: 'https://purepng.com/public/uploads/large/purepng.com-lemonlemonfruittastyyellowfoodadd-331522507302szdb0.png'
  },
  {
    name: 'Mango',
    price: 4,
    url: 'https://freepngimg.com/save/14747-mango-png-hd/640x480'
  }
]

export default products